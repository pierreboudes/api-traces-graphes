package fr.up13.api

import akka.actor.{ Actor, ActorLogging, Props }

final case class Etape(id: String, legende: String)
final case class EtapesSuivantes(liste: Vector[Etape])
final case class Lien(source: Int, target: Int, value: Int, trace: Int)
final case class Graphe(nodes: Vector[Etape], links: Vector[Lien])

object GraphBuilderActor {
  final case class GetEtapesSuivantes(name: String)
  final case class GetGraph(name: String)
  final case class GetSimpleGraph(name: String)

  def props: Props = Props[GraphBuilderActor]
}

class GraphBuilderActor(traces: List[Vector[String]], legende: Map[String, String]) extends Actor with ActorLogging {
  import GraphBuilderActor._

  val stupidgraph = Graphe(Vector(Etape("ah",""),Etape("bee", "")), Vector(Lien(0, 1, 8, 10)))

  def selectionner_traces(etapes: Vector[String]): List[Vector[String]] = {
    val ens_etapes =  etapes.toSet
    traces.filter(
      trace => trace.tail.exists(etape => ens_etapes contains etape)
    )
  }
  def legender(e: Etape): Etape = {
    val Etape(id,leg) = e
    val chaine = id.split('.')
    var etape = id
    if (chaine.length > 1) etape = chaine(1)
    Etape(id, legende.getOrElse(etape,""))
  }

  def construire_graphe(traces: List[Vector[String]]): Graphe = {
    val sommets = traces.toVector.map(_.tail.map(Etape(_,""))).flatten.toSet.toVector
    val arêtes = traces.zipWithIndex.map({
      case (xs, i) => xs.tail.sliding(2,1).map(
        l => Lien(sommets.indexOf(Etape(l(0), "")),sommets.indexOf(Etape(l(1), "")), xs.head.toInt, i))
    }).toVector.flatten
    Graphe(sommets.map(legender), arêtes)
  }

  def construire_graphe_simple(traces: List[Vector[String]]): Graphe = {
    val sommets = traces.toVector.map(_.tail.map(Etape(_, ""))).flatten.toSet.toVector
    val arêtes = traces.map({
      xs => xs.tail.sliding(2,1).map(l => Lien(sommets.indexOf(Etape(l(0), "")),sommets.indexOf(Etape(l(1), "")), 1, 0))
    }).toVector.flatten.toSet.toVector
    Graphe(sommets.map(legender), arêtes)
  }

  def successeurs(id: String, graphe: Graphe) : Option[EtapesSuivantes] = {
    val Graphe(sommets, liens) = graphe
    val i = sommets.indexOf(legender(Etape(id,"")))
    Some(EtapesSuivantes(liens.collect({
      case Lien(`i`, but, n, t) => sommets(but)
    }).toSet.toVector.map(legender)))
  }

  def etapes_suivantes(id: String): Option[EtapesSuivantes] = {
    val g = construire_graphe(selectionner_traces(Vector(id)))
    successeurs(id, g)
  }

  def receive: Receive = {
    case GetEtapesSuivantes(etape) =>
      println(s"GetEtapesSuivantes($etape)")
      println("GET Etapes suivantes", etape)
      sender() ! etapes_suivantes(etape)

    case GetGraph(name) => {
      println(s"GetGraph($name)")
      val etapes = name.split('/').toVector
      if (etapes.isEmpty) sender ! None
      else {
        sender() ! Some(construire_graphe(selectionner_traces(etapes)))
      }
    }

    case GetSimpleGraph(name) => {
      println(s"GetSimpleGraph($name)")
      val etapes = name.split('/').toVector
      if (etapes.isEmpty) sender ! None
      else {
        val graphe = construire_graphe_simple(selectionner_traces(etapes))
        sender() ! Some(graphe)
      }
    }
  }
}
