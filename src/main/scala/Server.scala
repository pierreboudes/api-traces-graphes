package fr.up13.api

import scala.concurrent.Await
import scala.concurrent.duration.Duration

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
// import scala.io.StdIn // pour attendre avec readline
import java.io._

object APIServer extends App with ApiRoutes {

  implicit val system: ActorSystem = ActorSystem("ApiTracesHttp")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  var traces: List[Vector[String]] = List()
  var legende: Map[String, String] = Map()

  if (args.length > 0) {
    val filename = args.toList(0)
    print(s"Lectur des données depuis ${filename}\n")
    val source = scala.io.Source.fromFile(filename, "utf-8")
    traces = source.getLines.toList.map(_.split(';').toVector) //.map(_.split('.').mkString(""))
    print(s"nombre de lignes de données lues : ${traces.length}\n")
    source.close()
  }

  if (args.length > 1) {
    val filename = args.toList(1)
    print(s"Lectur des étapes depuis ${filename}\n")
    val source = scala.io.Source.fromFile(filename, "utf-8")
    val etapes = source.getLines.toList
    source.close()
    print(s"nombre de lignes de données lues : ${etapes.length}\n")
    val tete +: etapescorps = etapes.map(_.split(";").toVector)
    val icode = tete.indexOf("CODE_ETAPE")
    val inom = tete.indexOf("LIBELLE_LONG_ETAPE")
    val ideb = tete.indexOf("PREMIERE ANNEE")
    val ifin = tete.indexOf("DERNIERE ANNEE")
    legende = etapescorps.map(v => (v(icode), v(inom) + " " + v(ideb) + "-" +
      (v(ifin).toInt + 1).toString)).toMap
  }

  val graphBuilderActor: ActorRef = system.actorOf(
    Props(classOf[GraphBuilderActor], traces, legende),
    "graphBuilderActor")
  lazy val routes: Route = graphRoutes

  Http().bindAndHandle(routes, "localhost", 8080)

  println(s"Serveur en ligne  http://localhost:8080/")

  // StdIn.readLine() // On laisse tourner le serveur jusqu'à ce que quelqu'un tape entrée
  //println("appelle system.shutdown")
  // system.shutdown

  Await.result(system.whenTerminated, Duration.Inf)
}
