package fr.up13.api
import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging

import scala.concurrent.duration._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model.{ ContentTypes, HttpEntity }
import akka.http.scaladsl.server.HttpApp

// import akka.http.scaladsl.server.directives.MethodDirectives.delete
import akka.http.scaladsl.server.directives.MethodDirectives.get
import akka.http.scaladsl.server.directives.MethodDirectives.post
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.directives.PathDirectives.path

import scala.concurrent.Future
import GraphBuilderActor._
import akka.pattern.ask
import akka.util.Timeout



trait ApiRoutes extends JsonSupport {


  // we leave these abstract, since they will be provided by the App
  implicit def system: ActorSystem
  implicit def ec = system.dispatcher
  lazy val log = Logging(system, classOf[ApiRoutes])

  // other dependencies that UserRoutes use
  def graphBuilderActor: ActorRef

  // Required by the `ask` (?) method below
  implicit lazy val timeout = Timeout(5.seconds) // usually we'd obtain the timeout from the system's configuration


  lazy val graphRoutes: Route =
    pathPrefix("api") {
      pathPrefix("poursuite") {
        path(Segment) { name =>
          get {
            val maybeEtapes: Future[Option[EtapesSuivantes]] =
              (graphBuilderActor ? GetEtapesSuivantes(name)).mapTo[Option[EtapesSuivantes]]
            /* rejectEmptyResponse(None) => 404 not Found */
            rejectEmptyResponse {
              complete(maybeEtapes)
            }
          }
        }
      } ~
      pathPrefix ("graphe" / Remaining) {
        p => get {
          println(s"GET graphe $p")
          val maybeGraph: Future[Option[Graphe]] =
            (graphBuilderActor ? GetGraph(p.toString)).mapTo[Option[Graphe]]
          rejectEmptyResponse {
            complete(maybeGraph)
          }
        }
      } ~
      pathPrefix ("graphesimple" / Remaining) {
        p => get {
          println(s"GET graphe simple $p")
          val maybeGraph: Future[Option[Graphe]] =
            (graphBuilderActor ? GetSimpleGraph(p.toString)).mapTo[Option[Graphe]]
          rejectEmptyResponse {
            complete(maybeGraph)
          }
        }
      } ~
      pathPrefix ("vue") {
        pathPrefix ("sankey" / Remaining) {
          p => get {
            println(s"GET Sankey $p")
            val html = s"""<!DOCTYPE html>
<html>
    <head>
        <title>Sankey suivi des primo (attention l'anonymisation réduit à 1 les petites cohortes)</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" type="text/css" href="/api/static//traces/css/style.css">
        <script src="/api/static/traces/js/js-sankey/d3.v4.js"></script>
        <script src="/api/static/traces/js/js-sankey/d3-sankey-traces.js"></script>
        <script src="/api/static/traces/js/draw_sankey.js"></script>
   </head>
    <body>
      <p id="chart"></p>

      <script>
        etape2sankey("${p}");
      </script>
    </body>
</html>
"""
            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, html))
          }
        } ~
        pathPrefix ("graphe" / Remaining) {
          p => get {
            println(s"GET graphe $p")
            val html = s"""<!DOCTYPE html>
<html>
    <head>
        <title>Sankey suivi des primo (attention l'anonymisation réduit à 1 les petites cohortes)</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" type="text/css" href="/api/static//traces/css/style.css">
        <script src="/api/static/traces/js/js-sankey/d3.v4.js"></script>
        <script src="/api/static/traces/js/js-sankey/d3-sankey-traces.js"></script>
        <script src="/api/static/traces/js/draw_sankey.js"></script>
   </head>
    <body>
      <p id="chart"></p>

      <script>
        etape2graphe("${p}");
      </script>
    </body>
</html>
"""
            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, html))
          }
        }
      } ~
      path("test") {
        /* pour tester ces petits marshallers capricieux */
        path ("graphe") {
          get {
            complete(Graphe(Vector(Etape("ah", "exclamation ah"),Etape("bee", "exclamation inconnue")), Vector(Lien(0, 1, 8, 10))))
          }
        } ~
        path ("lien") {
          get {
           complete(Some(Lien(0, 1, 10, 2)))
          }
        }
      } ~
      pathPrefix("static" / Remaining) { urlPath =>
        get {
          complete(StaticFile.serve(s"static/$urlPath"))
        }
      }
    }
}
