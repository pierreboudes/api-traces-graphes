package fr.up13.api

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport {
  // import the default encoders for primitive types (Int, String, Lists etc)
  import DefaultJsonProtocol._
  val i = 0
  implicit val etapeJsonFormat = jsonFormat2(Etape)
  implicit val etapesJsonFormat = jsonFormat1(EtapesSuivantes)
  implicit val lienJsonFormat = jsonFormat4(Lien)
  implicit val grapheJsonFormat = jsonFormat2(Graphe)
}
