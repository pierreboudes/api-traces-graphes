lazy val akkaHttpVersion = "10.1.3"
lazy val akkaVersion    = "2.5.13"

name := "graphe-traces-api"
version := "0.1"
cancelable in Global := true


  resolvers += "krasserm at bintray" at "http://dl.bintray.com/krasserm/maven"




lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "fr.up13",
      scalaVersion    := "2.12.6"
    )),
    name := "Web du Bar du Futur",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"          % akkaVersion,

    "com.github.krasserm" %% "streamz-akka-stream"  % "0.5.1",
    "org.apache.tika"     %  "tika-core"            % "1.14",
    "co.fs2"              %% "fs2-core"             % "0.9.2",
    "co.fs2"              %% "fs2-io"               % "0.9.2",

      "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % Test,
      "com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion     % Test,
      "org.scalatest"     %% "scalatest"            % "3.0.1"         % Test
    )
  )
