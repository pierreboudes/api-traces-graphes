# API Traces
fonctionne avec un fichier de données contenant les traces des étapes (voir mon projet apotraces pour la génération de ce fichier)

- installer sbt
- lancer sbt
`sbt`
- compiler/exécuter en passant en argument le fichier de traces et le fichier décrivant les étapes
`run traces.csv etapes.csv`
- utiliser l'API

Description de l'API :
- get `/api/poursuite/<etape>` retourne la liste des poursuites d'études possibles à partir de l'étape passée dans le chemin.
- get `/api/graphe/<etape1>/<etape2>/…` retourne un graphe dont les sommets sont les étapes et dont les arêtes sont des mouvements réel de cohortes avec leurs poids (anonymisés), en ne retenant que les cohortes étant passé par au moins une des étapes fournies.
- get `/api/graphesimple/<etape1>/<etape2>/…` retourne un graphe simple dont les sommets sont les étapes et dont les arêtes témoignent des passages de cohortes sans autre détail (il y a au plus une arête d'un sommet vers un autre).
- get `/api/vue/sankey/<etape1>/…` sert un document html contenant une visualisation svg sous forme de sankey du graphe correspondant à la suite de l'url.
- get `/api/vue/graphe/<etape1>/…` sert un document html contenant une visualisation svg d'un graphe simple correspondant à la suite de l'url.
